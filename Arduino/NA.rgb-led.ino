//set pin values (global)
int redPin= 4;
int greenPin = 3;
int bluePin = 2;

void setup() {
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

void loop() {
  setColor(255, 0, 0); // red
  delay(1000);
  setColor(245, 164, 2); // yellow
  delay(1000);
  setColor(0, 255, 110); //green
  delay(1000);
  setColor(0, 60, 255); // blue
  delay(1000);
  setColor(162, 3, 173); // Purple Color
  delay(1000);
 
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
